# Rick and Morty

<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url] -->

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="./src/assets/rickandmorty_logo.png" alt="Logo" width="160" height="160">
  </a>

</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

<img width="1440" alt="Rick and Morty" src="https://i.postimg.cc/tC56P2k9/Rick-and-Morty-Logo-and-Image.png">
</br>

The Rick and Morty API is a RESTful and GraphQL API based on the television show Rick and Morty. You will access to data about hundreds of characters, images, locations and episodes. The Rick and Morty API is filled with canonical information as seen on the TV show.

### Built With

- Client application

  - [React](https://reactjs.org)
  - [Apollo](https://redux.js.org)

<!-- GETTING STARTED -->

## Getting Started

### Docker

Docker is one of the prerequisites to make the application up and running. Make sure to install on your machine. Follow this [tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04) to install Docker on your Linux machine.

### Docker Compose

It will run the client(ReactJS)containers in the background. Follow this [tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04) to install Docker Compose on your Linux machine.

### Node.js and npm

Install Node.js and npm

### TypeScript

`npm install -g typescript`

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/digggy/rick-and-morty.git
   ```
2. Run the setup script
   ```sh
   cd rick-and-morty
   docker-compose up -d --build
   ```

<!-- USAGE EXAMPLES -->

## Usage

### Rick & Morty

Execute `yarn start` in the ` rick-and-morty` directory to run the app in a development mode.

### rick-and-morty
- 🚀 Deployed: https://quizzical-curran-4a2a22.netlify.app/ 

- The client application is exposed at http://localhost:3000 or http://localhost:3001

## Contact

Digdarshan Kunwar - kunwar.digdarshan@gmail.com

Project Link: [https://gitlab.com/digggy/rick-and-morty](https://gitlab.com/digggy/rick-and-morty)

## Acknowledgements

[Perdoo Team](https://www.perdoo.com/), Berlin Germany

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: assets/images/screenshot.png
