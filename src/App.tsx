import React from "react";
import ApplicationRoutes from "./config/ApplicationRoutes";
import { ApolloClient, InMemoryCache } from "@apollo/client";
import { ApolloProvider } from "@apollo/client/react";

const client = new ApolloClient({
  uri: "https://rickandmortyapi.com/graphql",
  cache: new InMemoryCache(),
});

function App() {
  return (
    <ApolloProvider client={client}>
      <ApplicationRoutes />
    </ApolloProvider>
  );
}

export default App;
