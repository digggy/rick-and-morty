import React from "react";

import { Result, Button } from "antd";

const Error = () => {
  //   notification.open({
  //     message: "Error ",
  //     description: "Error: Failed to fetch",
  //     icon: <FrownOutlined style={{ color: "#108ee9" }} />,
  //   });
  return (
    <Result
      status="warning"
      title="There are some problems with your operation."
      extra={
        <Button type="primary" key="console">
          Check your Internet Connection
        </Button>
      }
    />
  );
};

export default Error;
