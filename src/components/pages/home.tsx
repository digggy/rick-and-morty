import React, { useState } from "react";
import { Row, Col, Divider } from "antd";
import { TwitterTimelineEmbed } from "react-twitter-embed";
import { GiphyFetch } from "@giphy/js-fetch-api";
import { Gif, Grid } from "@giphy/react-components";

import "./home.css";
import Title from "antd/lib/typography/Title";

const giphyFetch = new GiphyFetch("sXpGFDGZs0Dv1mmNFvYaGUvYwKX0PWIh");

function GridDemo({ onGifClick }) {
  const fetchGifs = (offset: number) =>
    giphyFetch.search("rick and morty", { offset, limit: 8 });
  return (
    <>
      <Grid
        onGifClick={onGifClick}
        fetchGifs={fetchGifs}
        width={600}
        columns={3}
        gutter={6}
      />
    </>
  );
}

function Gifs() {
  const [modalGif, setModalGif] = useState();
  return (
    <>
      <GridDemo
        onGifClick={(gif, e) => {
          e.preventDefault();
          setModalGif(gif);
        }}
      />
      {modalGif && (
        <div
          className="gif-wrapper"
          onClick={(e) => {
            e.preventDefault();
            setModalGif(undefined);
          }}
        >
          <Gif gif={modalGif} width={200} />
        </div>
      )}
    </>
  );
}

const Home = () => {
  return (
    <>
      <div className="home-title">
        <Title level={2}> 💡 Your Feed </Title>
      </div>
      <Divider />

      <div className="homepage-container">
        <Row justify="space-around">
          <Col className="twitter-container">
            <TwitterTimelineEmbed
              sourceType="profile"
              screenName="RickandMorty"
              noScrollbar
            />
          </Col>
          <Col>
            <div className="secondary-container">
              <div className="gif-container">
                <Gifs />
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Home;
