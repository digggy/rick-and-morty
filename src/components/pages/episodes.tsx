import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import classNames from "classnames";
import {
  Empty,
  Timeline,
  Typography,
  Tag,
  Divider,
  Row,
  Col,
  Tooltip,
  Button,
} from "antd";
import { RightCircleFilled, LeftCircleFilled } from "@ant-design/icons";
import { useQuery, gql } from "@apollo/client";
import { Split } from "@geoffcox/react-splitter";

import Loading from "../loading";
import Error from "../error";
import "./episodes.css";

const { Title, Text } = Typography;

const GET_ALL_EPISODES = gql`
  query GetEpisodes($page: Int) {
    episodes(page: $page) {
      info {
        pages
        count
        next
      }
      results {
        id
        episode
        name
        air_date
        characters {
          id
          name
          image
        }
        created
      }
    }
  }
`;

const Episodes = () => {
  const [episodePagination, setEpisodePagination] = useState(1);
  const [episodePage, setEpisodePage] = useState(1);
  const [selectedEpisode, setselectedEpisode] = useState(null);

  const { loading, error, data, refetch } = useQuery(GET_ALL_EPISODES, {
    variables: { page: episodePage },
  });

  useEffect(() => {
    refetch({ page: episodePage });
  }, [episodePage, refetch]);

  if (error) {
    return <Error />;
  }
  if (loading) return <Loading />;
  const episodes = data.episodes?.results;
  const { pages, count } = data?.episodes.info;

  const handleEpisodeChange = (newEpisodePagination) => {
    setEpisodePagination(newEpisodePagination);
    const page = Math.ceil(newEpisodePagination / 4);

    if (episodePage <= pages && page !== episodePage) {
      if (newEpisodePagination > episodePagination) {
        setEpisodePage(page);
      } else {
        setEpisodePage(page);
      }
    }
  };

  const handleEpisodeSelection = (episodeId) => {
    setselectedEpisode(episodeId);
  };

  const startRange = ((episodePagination - 1) % 4) * 5;
  const allEpisodes = episodes.slice(startRange, startRange + 5).map((item) => {
    var cls = classNames("episode-timeline-item", {
      "episode-timeline-item-selected": item.id === selectedEpisode,
    });
    return (
      <Timeline.Item key={item.id}>
        <div className={cls} onClick={() => handleEpisodeSelection(item.id)}>
          <Title level={4}>{item.name}</Title>
          <Tag>{item.episode}</Tag> {item.air_date}
        </div>
      </Timeline.Item>
    );
  });

  const episode = episodes.filter((item) => item.id === selectedEpisode)[0];

  const charactersInEpisode = episode?.characters.map((character) => (
    <Link key={character.id} to={`/characters/${character.id}`}>
      <div className="episodes-character-card">
        {" "}
        <img width="100px" src={character.image} alt={character.name} />
        <Text className="episodes-character-card-text" strong>
          {character.name}
        </Text>
      </div>
    </Link>
  ));

  return (
    <Split initialPrimarySize="40%" resetOnDoubleClick>
      <div className="episode-primary-pane">
        <div>
          <Title level={3}>Episodes</Title>
          <Divider />
          <div>
            <Timeline>{allEpisodes}</Timeline>
          </div>
        </div>
        <Row justify="space-between">
          <Col>
            {episodePagination > 1 ? (
              <Tooltip title="Previous">
                <Button
                  type="primary"
                  shape="circle"
                  onClick={() => handleEpisodeChange(episodePagination - 1)}
                  icon={<LeftCircleFilled />}
                />
              </Tooltip>
            ) : null}
          </Col>
          <Col>
            <Tag>{episodePagination}</Tag>
          </Col>
          <Col>
            {episodePagination * 5 < count ? (
              <Tooltip title="Next">
                <Button
                  type="primary"
                  shape="circle"
                  onClick={() => handleEpisodeChange(episodePagination + 1)}
                  icon={<RightCircleFilled />}
                />
              </Tooltip>
            ) : null}
          </Col>
        </Row>
      </div>
      <div className="episode-secondary-pane">
        {selectedEpisode ? (
          <>
            <Title className="episode-secondary-pane-title" level={2}>
              Characters In Episode
            </Title>
            <div className="selected-episodes-characters">
              {charactersInEpisode}
            </div>
          </>
        ) : (
          <Empty />
        )}
      </div>
    </Split>
  );
};

export default Episodes;
