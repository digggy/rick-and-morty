import React from "react";
import { Typography, Tag, Card } from "antd";

const { Text, Title } = Typography;

interface CharacterDetailsProps {
  data: any;
}

function getEpisodeAirYear(episode: { air_date: number }) {
  if (episode.air_date) {
    return new Date(episode.air_date).getFullYear();
  }
  return "";
}

function CharacterDetails({ data }: CharacterDetailsProps) {
  const { name, status, species, gender, origin, location, episode } =
    data.character;

  const firstEpisode = episode ? episode[0] : null;
  const lastEpisode = episode ? episode[episode.length - 1] : null;

  return (
    <Card
      hoverable
      style={{ width: 320, borderRadius: "15px" }}
      cover={
        <img
          style={{ borderRadius: "15px" }}
          alt={data.character.name}
          src={data.character.image}
        />
      }
    >
      <div className="character-profile-spacing">
        <Title style={{ marginBottom: "0.1em" }} level={3}>
          {name}
        </Title>
        <Text>
          {firstEpisode && lastEpisode
            ? `Episodes: ${episode?.length} (${getEpisodeAirYear(
                firstEpisode
              )} - ${getEpisodeAirYear(lastEpisode)})`
            : null}
        </Text>
        <br />
        <Tag color={status.toLowerCase() === "alive" ? "success" : "error"}>
          {status}
        </Tag>
        <Tag color={species.toLowerCase() === "human" ? "geekblue" : "green"}>
          {species}
        </Tag>
        <Tag color={gender.toLowerCase() === "male" ? "#10239e" : "#c41d7f"}>
          {gender}
        </Tag>
        <div>
          <Text type="secondary">Origin</Text>
          <br />
          <Text>{origin.name}</Text>
        </div>
        <div>
          <Text type="secondary">Location</Text>
          <br />
          <Text>{location.name}</Text>
        </div>
      </div>
    </Card>
  );
}

export default CharacterDetails;
