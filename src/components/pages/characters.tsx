import React from "react";
import { useState } from "react";
import { useHistory } from "react-router";
import { useQuery, gql } from "@apollo/client";

import { Table, Row, Col, Button, Typography, Image, Pagination } from "antd";
import { EyeOutlined } from "@ant-design/icons";

import "./characters.css";
import Loading from "../loading";
import Tag from "../tag";
import Error from "../error";

const { Title, Text } = Typography;
const GET_CHARACTERS = gql`
  query GetAllCharacters($page: Int) {
    characters(page: $page) {
      info {
        count
        pages
        next
        prev
      }
      results {
        id
        name
        species
        image
        origin {
          name
        }
        location {
          name
        }
      }
    }
  }
`;

const Characters = () => {
  const history = useHistory();
  const [charaterPage, setcharaterPage] = useState(1);
  const { loading, error, data } = useQuery(GET_CHARACTERS, {
    variables: { page: charaterPage },
  });

  const handleClick = () => {
    history.push("/form/");
  };

  const handleActionClick = (id) => {
    history.push(`/characters/${id}`);
  };

  const handlePaginationChange = (page) => {
    setcharaterPage(page);
  };

  const columns = [
    {
      title: () => <Text strong> Name </Text>,
      dataIndex: "name",
      width: "20%",
      render: (text, record) => {
        return (
          <div className="characters-name-col">
            <Image
              className="characters-name-image"
              preview={false}
              width={50}
              alt={record.name}
              src={record.image}
            />
            <span style={{ paddingLeft: "10px" }}>{record.name}</span>
          </div>
        );
      },
    },
    {
      title: () => <Text strong> Species </Text>,
      dataIndex: "species",
      width: "15%",
      render: (tag) => <Tag tag={tag} />,
    },
    {
      title: () => <Text strong> Origin </Text>,
      dataIndex: "origin",
      width: "20%",
    },
    {
      title: () => <Text strong> Location </Text>,
      dataIndex: "location",
      width: "20%",
    },
    {
      title: () => <Text strong> Action </Text>,
      key: "key",
      width: "20%",
      dataIndex: "action",
      render: (text, record) => (
        <>
          <Button
            icon={<EyeOutlined />}
            size="middle"
            onClick={() => handleActionClick(record.key)}
            className="table-action-button"
          >
            View
          </Button>
        </>
      ),
    },
  ];
  if (error) {
    return <Error />;
  }
  if (loading) return <Loading />;

  const { info, results } = data?.characters;
  const characters = results?.map((character: any) => {
    return {
      key: character.id,
      name: character.name,
      species: character.species,
      location: character.location.name,
      origin: character.origin.name,
      image: character.image,
      action: null,
    };
  });

  return (
    <div>
      <Row gutter={[40, 0]}>
        <Col span={18}>
          <Title level={2}>Characters List</Title>
        </Col>
        <Col span={6}>
          <Button onClick={handleClick} block>
            Add Character
          </Button>
        </Col>
      </Row>
      <Row gutter={[40, 0]}>
        <Col span={24}>
          <Table columns={columns} dataSource={characters} pagination={false} />
          <Pagination
            className="location-pagination-wrapper"
            // size="small"
            pageSize={20}
            current={charaterPage}
            onChange={handlePaginationChange}
            total={info.count}
          />
        </Col>
      </Row>
    </div>
  );
};

export default Characters;
