import React, { useState } from "react";
import { useQuery, gql } from "@apollo/client";
import { Table, Pagination, Typography, Tooltip } from "antd";

import Loading from "../loading";
import Error from "../error";
import "./location.css";
import { Link } from "react-router-dom";

const GET_ALL_LOCATIONS = gql`
  query GetLocation($page: Int) {
    locations(page: $page) {
      info {
        pages
        count
        next
      }
      results {
        id
        name
        type
        dimension
        residents {
          id
          name
          image
        }
      }
    }
  }
`;
const columns = [
  { title: "Name", dataIndex: "name", key: "name" },
  { title: "Type", dataIndex: "type", key: "type" },
  { title: "Dimension", dataIndex: "dimension", key: "dimension" },
];

const Location = () => {
  const [locationPage, setlocationPage] = useState(1);
  const { loading, error, data } = useQuery(GET_ALL_LOCATIONS, {
    variables: { page: locationPage },
  });
  if (error) {
    return <Error />;
  }

  const info = data?.locations.info || {};
  const results = data?.locations.results || [];
  const allLocationsInPage = results?.map((location) => {
    return {
      key: location.id,
      name: location.name,
      type: location.type,
      dimension: location.dimension,
      residents: location.residents,
    };
  });
  const handlePaginationChange = (page) => {
    setlocationPage(page);
  };
  return (
    <>
      <Typography.Title level={3}>🌍 Locations </Typography.Title>
      {loading ? (
        <Loading />
      ) : (
        <div className="location-table-pagination-wrapper">
          <Table
            size={"small"}
            columns={columns}
            pagination={false}
            expandable={{
              expandedRowRender: (record) => {
                return record.residents.map((char) => {
                  const { name, image, id } = char;
                  return (
                    <Tooltip key={id} title={name} placement="bottom">
                      <Link to={`characters/${id}`}>
                        <img
                          className="locations-character-images"
                          src={image}
                          alt={name}
                        />
                      </Link>
                    </Tooltip>
                  );
                });
              },
              rowExpandable: (record) => record.name !== "Not Expandable",
            }}
            dataSource={allLocationsInPage}
          />
          <Pagination
            className="location-pagination-wrapper"
            pageSize={20}
            current={locationPage}
            onChange={handlePaginationChange}
            total={info.count}
          />
        </div>
      )}
    </>
  );
};

export default Location;
