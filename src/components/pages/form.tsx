import React, { useState } from "react";
import {
  Row,
  Col,
  Typography,
  Input,
  Form,
  Button,
  Select,
  Upload,
  message,
} from "antd";
import { InboxOutlined } from "@ant-design/icons";
import { useHistory } from "react-router";

const { Title } = Typography;
const { Dragger } = Upload;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

const imageUpload = {
  name: "file",
  multiple: false,
  action: "localhost",
  onChange(info) {
    const { status } = info.file;
    if (status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
  onDrop(e) {
    console.log("Dropped files", e.dataTransfer.files);
  },
};

const FormApp = () => {
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const handleSubmit = (values: any) => {
    setLoading(true);
    history.push("/characters");
  };

  return (
    <div>
      <Row gutter={[40, 0]}>
        <Col span={23}>
          <Title style={{ textAlign: "center" }} level={2}>
            Please Fill the Charater Form
          </Title>
        </Col>
      </Row>
      <Row gutter={[40, 0]}>
        <Col span={18}>
          <Form {...layout} onFinish={handleSubmit}>
            <Form.Item
              name="name"
              label="Name"
              rules={[
                {
                  required: true,
                  message: "Please input character's name",
                },
              ]}
            >
              <Input placeholder="Please Enter character's name" />
            </Form.Item>
            <Form.Item
              name="species"
              label="Species"
              rules={[
                {
                  required: true,
                  message: "Please select the character's species",
                  type: "array",
                },
              ]}
            >
              <Select
                mode="multiple"
                defaultValue="Human"
                placeholder="Please select the character's species"
              >
                <Select.Option value="Human">Human</Select.Option>
                <Select.Option value="Alien">Alien</Select.Option>
                <Select.Option value="Humanoid">Humanoid</Select.Option>
                <Select.Option value="Poopybutthole">
                  Poopybutthole
                </Select.Option>
                <Select.Option value="Mythological Creature">
                  Mythological Creature
                </Select.Option>
                <Select.Option value="Unknown">Unknown</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item
              name="origin"
              label="Origin"
              rules={[
                {
                  required: true,
                  message: "Please input the origin",
                },
              ]}
            >
              <Input placeholder="Please input the origin" />
            </Form.Item>
            <Form.Item
              name="location"
              label="Location"
              rules={[
                {
                  required: true,
                  message: "Please input the location",
                },
              ]}
            >
              <Input placeholder="Please input the location" />
            </Form.Item>
            <Form.Item name="avatar" label="Upload Image">
              <Dragger {...imageUpload}>
                <p className="ant-upload-drag-icon">
                  <InboxOutlined />
                </p>
                <p className="ant-upload-text">
                  Click or drag file to this area to upload
                </p>
                <p className="ant-upload-hint">
                  Please upload the character's avatar image.
                </p>
              </Dragger>
              ,
            </Form.Item>
            <div style={{ textAlign: "right" }}>
              <Button type="primary" loading={loading} htmlType="submit">
                Save
              </Button>{" "}
              <Button
                type="primary"
                htmlType="button"
                onClick={() => history.push("/list")}
              >
                Back
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </div>
  );
};

export default FormApp;
