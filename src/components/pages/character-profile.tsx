import React, { useState } from "react";
import { Tag, Row, Col, Divider, Timeline, Tooltip, Button } from "antd";

import { useParams } from "react-router-dom";
import CharacterDetails from "./character-details";
import { useQuery, gql } from "@apollo/client";
import Title from "antd/lib/typography/Title";
import { RightCircleFilled, LeftCircleFilled } from "@ant-design/icons";
import Error from "../error";
import Loading from "../loading";

const GET_CHARACTERS_DETAILS = gql`
  query GetCharacter($id: ID!) {
    character(id: $id) {
      id
      name
      status
      species
      gender
      origin {
        name
      }
      location {
        name
      }
      image
      episode {
        id
        name
        air_date
        episode
      }
      created
    }
  }
`;

interface CharacterProfileParams {
  id: string;
}

const CharacterProfile: React.FC<CharacterProfileParams> = () => {
  const { id } = useParams<{ id: string }>();
  const [episodePage, setEpisodePage] = useState(1);
  const { loading, error, data } = useQuery(GET_CHARACTERS_DETAILS, {
    variables: { id },
  });

  const handleEpisodeChange = (episodeNumber: number) => {
    setEpisodePage(episodeNumber);
  };
  if (error) {
    return <Error />;
  }
  const allEpisodes = data?.character?.episode
    .slice((episodePage - 1) * 6, episodePage * 6)
    .map((item) => {
      return (
        <Timeline.Item key={item.id}>
          <Title level={4}>{item.name}</Title>
          <Tag>{item.episode}</Tag> {item.air_date}
        </Timeline.Item>
      );
    });
  return (
    <div style={{ width: "900px", margin: "0 auto" }}>
      <Title> Character Profile</Title>
      {loading ? (
        <Loading />
      ) : (
        <Row>
          <Col flex={2}>
            <CharacterDetails data={data} />
          </Col>
          <Col style={{ width: "380px" }} flex={4}>
            <Title>Episodes</Title>
            <Divider />
            <div>
              <Timeline>{allEpisodes}</Timeline>
              <Row justify="space-between">
                <Col>
                  {episodePage > 1 ? (
                    <Tooltip title="Next">
                      <Button
                        type="primary"
                        shape="circle"
                        onClick={() => handleEpisodeChange(episodePage - 1)}
                        icon={<LeftCircleFilled />}
                      />
                    </Tooltip>
                  ) : null}
                </Col>
                <Col>
                  {episodePage * 6 <= data.character.episode.length ? (
                    <Tooltip title="Previous">
                      <Button
                        type="primary"
                        shape="circle"
                        onClick={() => handleEpisodeChange(episodePage + 1)}
                        icon={<RightCircleFilled />}
                      />
                    </Tooltip>
                  ) : null}
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      )}
    </div>
  );
};

export default CharacterProfile;
