import React from "react";
import { Link } from "react-router-dom";
import { Menu } from "antd";
import {
  GitlabFilled,
  VideoCameraFilled,
  EnvironmentFilled,
} from "@ant-design/icons";
import { useHistory } from "react-router";
import logo from "../../assets/rickandmorty_logo.png";

import "./sidebar.css";

interface SideNavProps {
  collapsed: boolean;
}

const SideNav: React.FC<SideNavProps> = ({ collapsed }) => {
  const history = useHistory();

  const handleCharactersClick = () => {
    history.push("/characters");
  };

  const handleLocationsClick = () => {
    history.push("/locations");
  };

  const handleEpisodesClick = () => {
    history.push("/episodes");
  };

  return (
    <div>
      <Link to="/home">
        <div className="logo-wrapper">
          <img src={logo} alt="Rick and Morty" style={{ maxWidth: "100%" }} />

          {!collapsed ? (
            <span className="logo-title">Rick and Morty</span>
          ) : null}
        </div>
      </Link>
      <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
        <Menu.Item key="1" onClick={handleCharactersClick}>
          <GitlabFilled />
          <span> Characters</span>
        </Menu.Item>
        <Menu.Item key="2" onClick={handleLocationsClick}>
          <EnvironmentFilled />
          <span> Locations</span>
        </Menu.Item>
        <Menu.Item key="3" onClick={handleEpisodesClick}>
          <VideoCameraFilled />
          <span> Episodes</span>
        </Menu.Item>
      </Menu>
    </div>
  );
};

SideNav.defaultProps = {
  collapsed: false,
};

export default SideNav;
