import React from "react";
import { Space, Spin, Typography } from "antd";

const Loading = () => {
  return (
    <div className="characters-loading-icon">
      <Space size="small" style={{ display: "flex", flexDirection: "column" }}>
        <Spin size="large" />
        <Typography.Text style={{ color: "#1890ff", fontSize: "1.2em" }}>
          Loading
        </Typography.Text>
      </Space>
    </div>
  );
};

export default Loading;
