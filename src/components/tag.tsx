import React from "react";
import { Tag as AntTag } from "antd";
import {
  UserOutlined,
  RedditOutlined,
  AndroidOutlined,
  QuestionCircleOutlined,
  RobotOutlined,
  GithubOutlined,
  WarningOutlined,
} from "@ant-design/icons";

const Tag = ({ tag }) => {
  let tagColor = "default";
  let icon;
  const tagName = tag.toLowerCase();

  if (tagName === "human") {
    tagColor = "geekblue";
    icon = <UserOutlined />;
  } else if (tagName === "alien") {
    tagColor = "green";
    icon = <RedditOutlined />;
  } else if (tagName === "unknown") {
    tagColor = "default";
    icon = <QuestionCircleOutlined />;
  } else if (tagName === "humanoid") {
    tagColor = "purple";
    icon = <AndroidOutlined />;
  } else if (tagName === "robot") {
    tagColor = "magenta";
    icon = <RobotOutlined />;
  } else if (tagName === "animal") {
    tagColor = "geekblue";
    icon = <GithubOutlined />;
  } else if (tagName === "disease") {
    tagColor = "red";
    icon = <WarningOutlined />;
  }
  return (
    <AntTag key={tag} color={tagColor} icon={icon}>
      {tag.toUpperCase()}
    </AntTag>
  );
};

export default Tag;
