import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { createMemoryHistory } from "history";
import { Router } from "react-router-dom";
import userEvent from "@testing-library/user-event";
import App from "./App";
import Loading from "./components/loading";
import { clear } from "console";

test("Init test for nav text", async () => {
  await act(async () => {
    render(<App />);
  });
  const linkElement = screen.getByText(/Rick /i);
  expect(linkElement).toBeInTheDocument();
});

test("App rendering/navigating", async () => {
  const history = createMemoryHistory();
  render(
    <Router history={history}>
      <App />
    </Router>
  );
  await waitFor(() => screen.getByText(/Characters/i));
  expect(screen.getByText(/Characters/i)).toBeInTheDocument();
  await waitFor(() => screen.getByText(/Loading/i));
  const leftClick = { button: 0 };
  userEvent.click(screen.getByText(/Locations/i), leftClick);
});

test("Loading Component", async () => {
  const history = createMemoryHistory();
  render(<Loading />);
  expect(screen.getByText(/Loading/i)).toBeInTheDocument();
});
